const env = require('./env.js');
const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
	host: env.host,
	port: env.port,
	dialect: env.dialect,
	operatorsAliases: false,

	pool: {
		max: env.max,
		min: env.pool.min,
		acquire: env.pool.acquire,
		idle: env.pool.idle
	}
});

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
//Models/tables
db.konsumens_tb = require('../model/konsumens_tb.model.js')(sequelize, Sequelize);
db.konsumens_tg = require('../model/konsumens_tg.model.js')(sequelize, Sequelize);
module.exports = db;