const env = {
  database: 'fan-api-test',
  username: 'root',
  password: '',
  host: 'localhost',
  dialect: 'mysql',
  port: '3310',
  pool: {
	  max: 5,
	  min: 0,
	  acquire: 30000,
	  idle: 10000
  }
};

module.exports = env;