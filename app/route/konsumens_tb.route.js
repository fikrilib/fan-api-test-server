module.exports = function(app) {
const konsumens_tb = require('../controller/konsumens_tb.controller.js');
	// Create
	app.post('/api/konsumens_tb', konsumens_tb.create);
	// Read
	app.get('/api/konsumens_tb', konsumens_tb.findAll);
	// Update
	app.put('/api/konsumens_tb', konsumens_tb.update);
	// Delete 
	app.delete('/api/konsumens_tb', konsumens_tb.delete);
}