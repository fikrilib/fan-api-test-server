module.exports = function(app) {
    const konsumens_tg = require('../controller/konsumens_tg.controller.js');
    // Create
    app.post('/api/konsumens_tg', konsumens_tg.create);
    // Read
    app.get('/api/konsumens_tg', konsumens_tg.findAll); 
    // Update
    app.put('/api/konsumens_tg', konsumens_tg.update);
    // Delete
    app.delete('/api/konsumens_tg', konsumens_tg.delete);
}