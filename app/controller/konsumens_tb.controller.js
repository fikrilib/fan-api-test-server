var nJwt = require('njwt');
const db = require('../config/db.config.js');
const Konsumens_tb = db.konsumens_tb;

// Create
exports.create = (req, res) => {	
	let konsumens_tb = nJwt.verify(req.body.datajson,"fan-api-test", 'HS512').body;
	Konsumens_tb.create(konsumens_tb).then(function (result) {
		res.status(200).json({msg:"Berhasil menambah data"});
	}).catch(function (err) {
		res.status(400).json({msg:err.errors});
	});

};

// Read
exports.findAll = (req, res) => {
	Konsumens_tb.findAll().then(konsumens_tb => {
		var jwt = nJwt.create(konsumens_tb,"fan-api-test","HS256");
		var token = jwt.compact();
		res.json(token);
	});
};

// Update 
exports.update = (req, res) => {
	let konsumens_tb = nJwt.verify(req.body.datajson,"fan-api-test", 'HS512').body;
	let id = konsumens_tb.id;
	Konsumens_tb.update(konsumens_tb, { where: {id: id} }).then(function (result) {
		res.status(200).json({msg:"Berhasil update data"});
	}).catch(function (err) {
		res.status(400).json({msg:err.errors});
	});
};
 
// Delete
exports.delete = (req, res) => {
	let konsumens_tb = nJwt.verify(req.body.datajson,"fan-api-test", 'HS512').body;
	console.log(konsumens_tb);
	let id = konsumens_tb.id;
	Konsumens_tb.destroy({where: { id: id }}).then(function (result) {
		res.status(200).json({msg:"Berhasil delete data"});
	}).catch(function (err) {
		res.status(400).json({msg:err});
	});
};