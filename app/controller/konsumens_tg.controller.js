var nJwt = require('njwt');
const db = require('../config/db.config.js');
const Konsumens_tg = db.konsumens_tg;

// Create
exports.create = (req, res) => {	
	let konsumens_tg = nJwt.verify(req.body.datajson,"fan-api-test", 'HS512').body;
	Konsumens_tg.create(konsumens_tg).then(function (result) {
		res.status(200).json({msg:"Berhasil menambah data"});
	}).catch(function (err) {
		res.status(400).json({msg:err.errors});
	});

};

// Read
exports.findAll = (req, res) => {
	Konsumens_tg.findAll().then(konsumens_tg => {
		var jwt = nJwt.create(konsumens_tg,"fan-api-test","HS256");
		var token = jwt.compact();
		res.json(token);
	});
};

// Update 
exports.update = (req, res) => {
	let konsumens_tg = nJwt.verify(req.body.datajson,"fan-api-test", 'HS512').body;
	let id = konsumens_tg.id;
	Konsumens_tg.update(konsumens_tg, { where: {id: id} }).then(function (result) {
		res.status(200).json({msg:"Berhasil update data"});
	}).catch(function (err) {
		res.status(400).json({msg:err.errors});
	});
};
 
// Delete
exports.delete = (req, res) => {
	let konsumens_tg = nJwt.verify(req.body.datajson,"fan-api-test", 'HS512').body;
	console.log(konsumens_tg);
	let id = konsumens_tg.id;
	Konsumens_tg.destroy({where: { id: id }}).then(function (result) {
		res.status(200).json({msg:"Berhasil delete data"});
	}).catch(function (err) {
		res.status(400).json({msg:err});
	});
};